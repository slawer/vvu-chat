package com.vvuchat.vvuchat.Model;

import android.content.Context;
import android.content.SharedPreferences;

import com.vvuchat.vvuchat.R;


public class PrefConfig {
    private SharedPreferences sharedPreferences;
    private Context context;

    public PrefConfig(Context context) {
        this.context = context;
        sharedPreferences=context.getSharedPreferences(context.getString(R.string.pref_file),Context.MODE_PRIVATE);
    }

    public void writeLoginStatus(boolean status){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(context.getString(R.string.pref_login_status),status);
        editor.commit();
    }

    public  boolean readLoginStatus(){
        return sharedPreferences.getBoolean(context.getString(R.string.pref_login_status),false);
    }

    public void writeEmail(String email){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_email),email);
        editor.commit();
    }

    public void writeType(String type){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_type),type);
        editor.commit();
    }

    public void writeSchool(String school){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_school),school);
        editor.commit();
    }

    public void writeDepartment(String dept){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_department),dept);
        editor.commit();
    }


    public String readDepartment(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_department),"default");
    }

    public void writePosition(String position){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_position),position);
        editor.commit();
    }

    public String readType(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_type),"default");
    }

    public String readPosition(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_position),"User");
    }

    public String readSchool(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_school),"school");
    }
}
