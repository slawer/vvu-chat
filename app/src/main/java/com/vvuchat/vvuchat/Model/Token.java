package com.vvuchat.vvuchat.Model;

/**
 * Created by nigma on 21/01/2018.
 */

public class Token {
    public String token;

    public Token() {
    }

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
