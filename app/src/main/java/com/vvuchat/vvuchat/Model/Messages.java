package com.vvuchat.vvuchat.Model;

/**
 * Created by nigma on 29/03/2018.
 */

public class Messages {

    private String message;
    private String sender;
    private String datetime;
    private String senderId;
    private String position;
    private String receiver;
    //private String message;


    public Messages() {
    }

    public Messages(String message, String sender, String datetime, String senderId, String position, String receiver) {
        this.message = message;
        this.sender = sender;
        this.datetime = datetime;
        this.senderId = senderId;
        this.position = position;
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
}
