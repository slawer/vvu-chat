package com.vvuchat.vvuchat.Model;

import java.util.List;

/**
 * Created by nigma on 21/01/2018.
 */

public class FCMResponse {

    public long multicast_id;
    public int success;
    public int failure;
    public int canonica_ids;
    public List<Result> results;

    public FCMResponse() {
    }

    public FCMResponse(long multicast_id, int success, int failure, int canonica_ids, List<Result> results) {
        this.multicast_id = multicast_id;
        this.success = success;
        this.failure = failure;
        this.canonica_ids = canonica_ids;
        this.results = results;
    }

    public long getMulticast_id() {
        return multicast_id;
    }

    public void setMulticast_id(long multicast_id) {
        this.multicast_id = multicast_id;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }

    public int getCanonica_ids() {
        return canonica_ids;
    }

    public void setCanonica_ids(int canonica_ids) {
        this.canonica_ids = canonica_ids;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
