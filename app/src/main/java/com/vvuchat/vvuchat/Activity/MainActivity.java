package com.vvuchat.vvuchat.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vvuchat.vvuchat.Model.PrefConfig;
import com.vvuchat.vvuchat.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public Button btnSignin, btnSignup;
    private EditText edtEmail, edtPassword;
    private TextView txtResetPwd;

    public PrefConfig prefConfig;

    private FirebaseDatabase database;
    private DatabaseReference mUsers;
    private FirebaseAuth mAuth;

    private ProgressDialog mProgress;
    // String user_id = "";
    String usertype = "";
    String user_school = "";
    String user_department = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgress = new ProgressDialog(this);
        prefConfig = new PrefConfig(this);

        //Firebase
        database = FirebaseDatabase.getInstance();

        mAuth = FirebaseAuth.getInstance();


        mUsers = database.getReference().child("Users");


        edtEmail = findViewById(R.id.txtlEmail);
        edtPassword = findViewById(R.id.txtLPass);
        txtResetPwd = findViewById(R.id.txtViewResetPwd);

        btnSignin = findViewById(R.id.btnSignin);
        btnSignup = findViewById(R.id.btnSignup);

        btnSignin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);
        txtResetPwd.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignin:
                checkLogin();
                break;

            case R.id.btnSignup:
                //signUp();
                signUpPop();
                break;

            case R.id.txtViewResetPwd:
                resetPassword();
                break;
        }
    }

    private void checkUserExist() {
        if (mAuth.getCurrentUser() != null) {
            final String user_id = mAuth.getCurrentUser().getUid();

            mUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(user_id)) {
                        usertype = dataSnapshot.child(user_id).child("type").getValue(String.class);
                        user_school = dataSnapshot.child(user_id).child("school").getValue(String.class);
                        user_department = dataSnapshot.child(user_id).child("department").getValue(String.class);

                        Toast.makeText(MainActivity.this, ""+user_school, Toast.LENGTH_SHORT).show();
                        prefConfig.writeSchool(user_school);
                        prefConfig.writeDepartment(user_department);
                        prefConfig.writeLoginStatus(true);
                        prefConfig.writeType(usertype);

                        //student subscription
                        if (user_school.equals(getString(R.string.sob)) && usertype.equals("student")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobStudent");
                        }
                        if (user_school.equals(getString(R.string.fos)) && usertype.equals("student")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosStudent");
                        }
                        if (user_school.equals(getString(R.string.fass)) && usertype.equals("student")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassStudent");
                        }
                        if (user_school.equals(getString(R.string.sotm)) && usertype.equals("student")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/themsaAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/themsaStudent");
                        }
                        if (user_school.equals(getString(R.string.soe)) && usertype.equals("student")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeStudent");
                        }

                        //faculty subsription
                        if (user_school.equals(getString(R.string.soe)) && usertype.equals("faculty")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeFaculty");
                        }

                        if (user_school.equals(getString(R.string.fos)) && usertype.equals("faculty")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosFaculty");
                        }

                        if (user_school.equals(getString(R.string.sob)) && usertype.equals("faculty")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobFaculty");
                        }

                        if (user_school.equals(getString(R.string.fass)) && usertype.equals("faculty")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassFaculty");
                        }

                        if (user_school.equals(getString(R.string.sotm)) && usertype.equals("faculty")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/themsaFaculty");
                        }

                        //staffs
                        if (user_school.equals(getString(R.string.soe)) && usertype.equals("staff")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Staffs");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeStaff");
                        }

                        if (user_school.equals(getString(R.string.fos)) && usertype.equals("staff")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Staffs");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fosStaff");
                        }

                        if (user_school.equals(getString(R.string.sob)) && usertype.equals("staff")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Staffs");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/sobStaff");
                        }

                        if (user_school.equals(getString(R.string.fass)) && usertype.equals("staff")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Staffs");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/fassStaff");
                        }

                        if (user_school.equals(getString(R.string.sotm)) && usertype.equals("staff")) {
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/All");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/Faculty");
                            FirebaseMessaging.getInstance().subscribeToTopic("/topics/themsaFaculty");
                        }

                        prefConfig.writeType(usertype);
                        prefConfig.writeSchool(user_school);
                        prefConfig.writeLoginStatus(true);

                        Intent mainInent = new Intent(MainActivity.this, MessagesActivity.class);
                        mainInent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainInent);
                    } else {
                        Toast.makeText(MainActivity.this, "User does not exist.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void checkLogin() {
        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if (!email.isEmpty() && password.length() < 6 && password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
        } else if (email.isEmpty() && password.length() < 6 && password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
        } else if (email.isEmpty() && password.length() < 6 && !password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
        } else if (!email.isEmpty() && password.length() < 6 && !password.isEmpty()) {
            Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
        } else if (!email.isEmpty() && !password.isEmpty()) {
            mProgress.setMessage("Please wait...");
            mProgress.show();
            if (email.endsWith("@vvu.edu.gh") || email.endsWith("@st.vvu.edu.gh")) {
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mProgress.dismiss();
                        checkUserExist();
                    } else {
                        mProgress.dismiss();
                        Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

    }


    public void signUpPop() {
        Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.sign_options);

        LinearLayout student = dialog.findViewById(R.id.layout);
        student.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, SignUp.class));
            dialog.dismiss();
            //Toast.makeText(MainActivity.this, "Student Touched", Toast.LENGTH_SHORT).show();
        });

        LinearLayout lecturer = dialog.findViewById(R.id.layout1);
        lecturer.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, FacultySignUp.class));
            dialog.dismiss();
            //Toast.makeText(MainActivity.this, "Lecturer Touched", Toast.LENGTH_SHORT).show();
        });

        LinearLayout staff = dialog.findViewById(R.id.layout2);
        staff.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, StaffSignUp.class));
            dialog.dismiss();
            //Toast.makeText(MainActivity.this, "Staff Touched", Toast.LENGTH_SHORT).show();
        });

        dialog.create();
        dialog.show();
    }

    public void resetPassword() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);


        LayoutInflater inflater = LayoutInflater.from(this);
        View reset_layout = inflater.inflate(R.layout.reset_password_layout, null);

        EditText email = reset_layout.findViewById(R.id.edtEmail);
        dialog.setView(reset_layout);

        dialog.setPositiveButton("RESET", (dialogInterface, i) ->
        {
            mProgress.setMessage("Please wait...");
            mProgress.show();
            if (email.getText().toString().isEmpty()) {
                mProgress.dismiss();
                Toast.makeText(MainActivity.this, "Please enter a valid email", Toast.LENGTH_SHORT).show();
            } else if (!email.getText().toString().isEmpty()) {
                mAuth.sendPasswordResetEmail(email.getText().toString().trim())
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(MainActivity.this, "Reset Password link sent", Toast.LENGTH_LONG).show();
                                dialogInterface.dismiss();
                                mProgress.dismiss();
                            }
                        })
                        .addOnFailureListener(e -> {
                            Toast.makeText(MainActivity.this, "Invalid email entered. Please enter valid email to proceed", Toast.LENGTH_LONG).show();
                            dialogInterface.dismiss();
                            mProgress.dismiss();

                        });
            }
        });

        dialog.setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.dismiss());


        dialog.create();
        dialog.show();
    }
}
