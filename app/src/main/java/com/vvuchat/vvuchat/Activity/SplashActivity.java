package com.vvuchat.vvuchat.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.vvuchat.vvuchat.Model.PrefConfig;
import com.vvuchat.vvuchat.R;

public class SplashActivity extends AppCompatActivity {

    ImageView imageView;
    Handler handler;
    Animation startRotation;
    PrefConfig prefConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
         imageView=findViewById(R.id.imageView);

        startRotation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_object);
        imageView.startAnimation(startRotation);

        prefConfig = new PrefConfig(this);

        handler = new Handler();
        handler.postDelayed(() -> {
            if (prefConfig.readLoginStatus() == true) {
                Intent intent = new Intent(SplashActivity.this, MessagesActivity.class);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(intent);
                finish();
            }
            else if (prefConfig.readLoginStatus() == false) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(intent);
                finish();
            }

        }, 4000);


    }
}
