package com.vvuchat.vvuchat.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vvuchat.vvuchat.Fragment.AdminMsgs;
import com.vvuchat.vvuchat.Fragment.DeptMsgs;
import com.vvuchat.vvuchat.Model.PrefConfig;
import com.vvuchat.vvuchat.R;

public class MessagesActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigationView;
    Toolbar toolbar;
    public static FirebaseAuth mAuth;

    PrefConfig prefConfig;

    DatabaseReference mDatabase, mDataStud, mDataFaculty, mDataStaff;

    ProgressDialog mProgress;


    FirebaseUser mCurrentUser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);

        prefConfig=new PrefConfig(MessagesActivity.this);
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mProgress = new ProgressDialog(this);

        final String user_id = mAuth.getCurrentUser().getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Messages");

        mDataStud = FirebaseDatabase.getInstance().getReference().child("StudentMessages");
        mDataFaculty = FirebaseDatabase.getInstance().getReference().child("FacultyMessages");
        mDataStaff = FirebaseDatabase.getInstance().getReference().child("StaffMessages");



        navigationView = findViewById(R.id.navigation);

        navigationView.setOnNavigationItemSelectedListener(this);

        setDefaultFragment();


        mDatabase.keepSynced(true);
        mDataStud.keepSynced(true);
        mDataFaculty.keepSynced(true);
        mDataStaff.keepSynced(true);

    }

    @Override
    protected void onStart() {
        super.onStart();

        //mAuth.addAuthStateListener(mAuthListener);
    }

    private void setDefaultFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, AdminMsgs.newInstance());
        transaction.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_admin:

                selectedFragment = AdminMsgs.newInstance();
                break;

            case R.id.navigation_dept:
                selectedFragment = DeptMsgs.newInstance();
                break;

        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
        return true;
    }

    private void signOut() {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage("Do you want to logout?")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("Deans");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("Staffs");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("Students");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("Faculty");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("All");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/sobAll");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/sobStudent");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fosAll");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fosStudent");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fassAll");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fassStudent");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/themsaAll");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/themsaStudent");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/soeAll");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/soeStudent");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/soeFaculty");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fosFaculty");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/sobFaculty");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fassFaculty");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/themsaFaculty");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/soeStaff");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fosStaff");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/sobStaff");

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/fassStaff");


                            prefConfig.writeLoginStatus(false);
                            prefConfig.writeEmail("User");
                            prefConfig.writeType("default");
                            prefConfig.writeSchool("school");
                            prefConfig.writeDepartment("default");


                            mAuth.signOut();
                            Intent intent = new Intent(MessagesActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        }
                )
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == R.id.action_logout) {
            //startActivity(new Intent(Write.this,Posts.class));
            signOut();
        }

        return super.onOptionsItemSelected(item);
    }
}



