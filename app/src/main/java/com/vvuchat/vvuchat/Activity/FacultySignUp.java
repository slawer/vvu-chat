package com.vvuchat.vvuchat.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vvuchat.vvuchat.R;

public class FacultySignUp extends AppCompatActivity implements View.OnClickListener {
    private EditText edtName;
    private EditText edtStudID;
    private EditText edtPassword;
    private EditText edtConPassword;
    private EditText edtEmail;
    private Button btnSignup;
    private Spinner spinProgram;
    private Spinner spinSchool;

    private FirebaseDatabase database;
    private DatabaseReference mUsers, mFaculty, mStaff;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private ProgressDialog mProgress;

    private String program, dept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_sign_up);

        mProgress = new ProgressDialog(this);

        //Firebase
        database = FirebaseDatabase.getInstance();
        mUsers = database.getReference().child("Users");


        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };

        edtName = findViewById(R.id.txtName);
        edtEmail = findViewById(R.id.txtSEmail);
        edtStudID = findViewById(R.id.txtStudID);
        edtPassword = findViewById(R.id.txtPassword);
        edtConPassword = findViewById(R.id.txtConPassword);

        btnSignup = (Button) findViewById(R.id.btnSignUp);

        spinProgram = (Spinner) findViewById(R.id.spinnerProgram);
        spinSchool = (Spinner) findViewById(R.id.spinnerSchool);
        //create array adapter for the spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.schools, android.R.layout.simple_spinner_item);
        //set the layout for the dropdown spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the adapter for the Spinner
        spinProgram.setAdapter(adapter);
        //Code that gets the position of the slected item
        int postion = spinProgram.getSelectedItemPosition();
        //The code that selects the item
        spinProgram.setSelection(postion);//selects the first item


        spinProgram.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //split=position+1;

                //Sets the value chosen in the spinner to the text===================================================================
                program = (String) spinProgram.getSelectedItem();//this is very important to get the value to be played in the database
                //==================================================================================================================================
                selectingProg();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSignup.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                checkUserExist();
                //signingUp();
                break;
        }
    }

    private void selectingProg() {
        if (program.equals(getString(R.string.sob))) {
            //create array adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deptSoB, android.R.layout.simple_spinner_item);
            //set the layout for the dropdown spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //set the adapter for the Spinner
            spinSchool.setAdapter(adapter);
            //Code that gets the position of the slected item
            int postion = spinSchool.getSelectedItemPosition();
            //The code that selects the item
            spinSchool.setSelection(postion);//selects the first item


            spinSchool.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    //split=position+1;

                    //Sets the value chosen in the spinner to the text===================================================================
                    dept = (String) spinSchool.getSelectedItem();//this is very important to get the value to be played in the database
                    //==================================================================================================================================
                    //Toast.makeText(FacultySignUp.this, ""+dept, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else if (program.equals(getString(R.string.fos))) {
            //create array adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deptFos, android.R.layout.simple_spinner_item);
            //set the layout for the dropdown spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //set the adapter for the Spinner
            spinSchool.setAdapter(adapter);
            //Code that gets the position of the slected item
            int postion = spinSchool.getSelectedItemPosition();
            //The code that selects the item
            spinSchool.setSelection(postion);//selects the first item


            spinSchool.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    //split=position+1;

                    //Sets the value chosen in the spinner to the text===================================================================
                    dept = (String) spinSchool.getSelectedItem();//this is very important to get the value to be played in the database
                    //==================================================================================================================================
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else if (program.equals(getString(R.string.soe))) {
            //create array adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deptSoe, android.R.layout.simple_spinner_item);
            //set the layout for the dropdown spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //set the adapter for the Spinner
            spinSchool.setAdapter(adapter);
            //Code that gets the position of the slected item
            int postion = spinSchool.getSelectedItemPosition();
            //The code that selects the item
            spinSchool.setSelection(postion);//selects the first item


            spinSchool.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    //split=position+1;

                    //Sets the value chosen in the spinner to the text===================================================================
                    dept = (String) spinSchool.getSelectedItem();//this is very important to get the value to be played in the database
                    //==================================================================================================================================
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else if (program.equals(getString(R.string.sotm))) {
            //create array adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deptSotm, android.R.layout.simple_spinner_item);
            //set the layout for the dropdown spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //set the adapter for the Spinner
            spinSchool.setAdapter(adapter);
            //Code that gets the position of the slected item
            int postion = spinSchool.getSelectedItemPosition();
            //The code that selects the item
            spinSchool.setSelection(postion);//selects the first item


            spinSchool.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    //split=position+1;

                    //Sets the value chosen in the spinner to the text===================================================================
                    dept = (String) spinSchool.getSelectedItem();//this is very important to get the value to be played in the database
                    //==================================================================================================================================
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else if (program.equals(getString(R.string.fass))) {
            //create array adapter for the spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deptFass, android.R.layout.simple_spinner_item);
            //set the layout for the dropdown spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //set the adapter for the Spinner
            spinSchool.setAdapter(adapter);
            //Code that gets the position of the slected item
            int postion = spinSchool.getSelectedItemPosition();
            //The code that selects the item
            spinSchool.setSelection(postion);//selects the first item


            spinSchool.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    //split=position+1;

                    //Sets the value chosen in the spinner to the text===================================================================
                    dept = (String) spinSchool.getSelectedItem();//this is very important to get the value to be played in the database
                    //==================================================================================================================================
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


    }

    private void checkUserExist() {
        if (edtEmail.getText().toString().equals("")) {
            Toast.makeText(FacultySignUp.this, "All fields are required", Toast.LENGTH_SHORT).show();
        } else {
            mAuth.fetchProvidersForEmail(edtEmail.getText().toString())
                    .addOnCompleteListener(task -> {
                        boolean check = !task.getResult().getProviders().isEmpty();

                        if (!check) {
                            signingUp();
                        } else {
                            Toast.makeText(FacultySignUp.this, "User email already exist", Toast.LENGTH_SHORT).show();
                            mProgress.dismiss();
                        }
                    });

        }
    }

    private void signingUp() {
        if (edtPassword.getText().toString().length() < 6 && !edtPassword.getText().toString().isEmpty()) {
            edtPassword.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            mProgress.dismiss();
            Toast.makeText(FacultySignUp.this, "Password is too short", Toast.LENGTH_LONG).show();
        }
        if (!(edtStudID.getText().toString().length() <= 13)) {
            edtStudID.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            mProgress.dismiss();
            Toast.makeText(FacultySignUp.this, "Check your ID", Toast.LENGTH_LONG).show();
        }
        if (!edtPassword.getText().toString().equals(edtConPassword.getText().toString())) {
            edtPassword.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            edtConPassword.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            mProgress.dismiss();
            Toast.makeText(FacultySignUp.this, "Passwords do not match", Toast.LENGTH_LONG).show();
        } else if (!edtPassword.getText().toString().isEmpty() && !edtEmail.getText().toString().isEmpty() && !edtConPassword.getText().toString().isEmpty() && !edtStudID.getText().toString().isEmpty()) {
            mProgress.setMessage("Signing Up");
            mProgress.show();
            if (edtEmail.getText().toString().endsWith("@vvu.edu.gh") && edtStudID.getText().toString().startsWith("vvu") || edtStudID.getText().toString().startsWith("Vvu") || edtStudID.getText().toString().startsWith("VVU")) {
                mAuth.createUserWithEmailAndPassword(edtEmail.getText().toString(), edtPassword.getText().toString()).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String usertype = "faculty";

                        if (usertype.equals("faculty")) {
                            // FirebaseMessaging.getInstance().subscribeToTopic("/topics/Students");

                            String user_id = mAuth.getCurrentUser().getUid();
                            DatabaseReference current_user_db = mUsers.child(user_id);

                            current_user_db.child("_id").setValue(edtStudID.getText().toString());
                            current_user_db.child("name").setValue(edtName.getText().toString());
                            current_user_db.child("email").setValue(edtEmail.getText().toString());
                            current_user_db.child("school").setValue(program);
                            current_user_db.child("department").setValue(dept);
                            current_user_db.child("uid").setValue(user_id);
                            current_user_db.child("type").setValue(usertype);
                            //current_user_db.child("image").setValue("default");
                            Toast.makeText(FacultySignUp.this, "Sign Up Successful", Toast.LENGTH_LONG).show();
                            mProgress.dismiss();

                            Intent mainInent = new Intent(FacultySignUp.this, MainActivity.class);
                            mainInent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainInent);
                            finish();
                        } else {

                        }
                    }
                    // Toast.makeText(MainActivity.this,"FacultySignUp Successful",Toast.LENGTH_LONG);
                });
            } else {
                edtEmail.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
                edtStudID.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
                Toast.makeText(FacultySignUp.this, "Please enter a valid VVU email and employee number.", Toast.LENGTH_LONG).show();
                mProgress.dismiss();
            }

        } else if (edtName.getText().toString().isEmpty() || edtEmail.getText().toString().isEmpty() || edtConPassword.getText().toString().isEmpty() || edtPassword.getText().toString().isEmpty() || edtStudID.getText().toString().isEmpty()) {
            edtEmail.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            edtConPassword.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            edtName.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            edtStudID.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            edtPassword.setBackground(getResources().getDrawable(R.drawable.et_red_bg));
            Toast.makeText(FacultySignUp.this, "All fields must not be empty!", Toast.LENGTH_SHORT).show();
        }
    }
}
