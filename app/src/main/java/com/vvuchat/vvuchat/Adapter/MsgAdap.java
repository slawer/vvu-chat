package com.vvuchat.vvuchat.Adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.Query;
import com.vvuchat.vvuchat.Model.Messages;
import com.vvuchat.vvuchat.R;

public class MsgAdap extends FirebaseRecyclerAdapter<Messages,MsgAdap.MsgViewHolder> {

    //private List<Messages> mMessageList;
    private FirebaseAuth mAuth;

    /**
     * @param modelClass      Firebase will marshall the data at a location into
     *                        an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list.
     *                        You will be responsible for populating an instance of the corresponding
     *                        view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
     *                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public MsgAdap(Class<Messages> modelClass, int modelLayout, Class<MsgViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
    }


    @Override
    public MsgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mAuth= FirebaseAuth.getInstance();
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.receivedmessages,parent,false);
        return new MsgViewHolder(view);
    }

    public static class MsgViewHolder extends RecyclerView.ViewHolder{

        public TextView messageText;
        public TextView messageUser;
        public TextView messageTime;
        public RelativeLayout real;
        public MsgViewHolder(View view) {
            super(view);

            messageText = (TextView) view.findViewById(R.id.message_text);
            messageUser = (TextView) view.findViewById(R.id.message_user);
            messageTime = (TextView) view.findViewById(R.id.message_time);
            real = view.findViewById(R.id.realLay);

        }
    }

    @Override
    protected void populateViewHolder(MsgViewHolder viewHolder, Messages model, int position) {

        String current_user_id=mAuth.getCurrentUser().getUid();
        try {
            String senderID=model.getSenderId();

            if (senderID.equals(current_user_id)){
                viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                viewHolder.messageText.setTextColor(Color.WHITE);
                viewHolder.messageTime.setTextColor(Color.WHITE);
                viewHolder.messageUser.setTextColor(Color.WHITE);
            }
            else{
                //viewHolder.real.setBackgroundColor(Color.GREEN);
                viewHolder.real.setBackgroundResource(R.drawable.text_background);
                viewHolder.messageText.setTextColor(Color.WHITE);
                viewHolder.messageUser.setTextColor(Color.WHITE);
                viewHolder.messageTime.setTextColor(Color.WHITE);
            }

            viewHolder.messageText.setText(model.getMessage());
            viewHolder.messageUser.setText(model.getSender());
            viewHolder.messageTime.setText(model.getDatetime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
