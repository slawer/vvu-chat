package com.vvuchat.vvuchat.Fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.vvuchat.vvuchat.Adapter.MsgAdap;
import com.vvuchat.vvuchat.Model.Messages;
import com.vvuchat.vvuchat.Model.PrefConfig;
import com.vvuchat.vvuchat.R;

public class AdminMsgs extends Fragment {
    View myFragment;
    RecyclerView recyclerView;
    LinearLayoutManager mLinearLayout;
    FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder> adapter;

    public DatabaseReference mDataMsgStaff,mDataMsgStudent,mDataMsgFaculty,mUsers;

    private ProgressDialog mProgress;

    private FirebaseAuth mAuth;
    PrefConfig prefConfig;

    String user_id="";
    String usertype= "";

    Query query;
    public AdminMsgs() {
    }

    public static AdminMsgs newInstance() {
        AdminMsgs fragment = new AdminMsgs();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragment=inflater.inflate(R.layout.fragment_admin_msgs, container, false);

        recyclerView=myFragment.findViewById(R.id.recyclerAdmin);
        mLinearLayout = new LinearLayoutManager(getActivity());

        prefConfig=new PrefConfig(getActivity());


        mAuth= FirebaseAuth.getInstance();
        mProgress=new ProgressDialog(getActivity());

        user_id=mAuth.getCurrentUser().getUid();
        usertype=prefConfig.readType();

        mDataMsgStudent= FirebaseDatabase.getInstance().getReference().child("StudentMessages");
        mDataMsgFaculty= FirebaseDatabase.getInstance().getReference().child("FacultyMessages");
        mDataMsgStaff= FirebaseDatabase.getInstance().getReference().child("StaffMessages");

        mDataMsgFaculty.keepSynced(true);
        mDataMsgStaff.keepSynced(true);
        mDataMsgStudent.keepSynced(true);


        mUsers= FirebaseDatabase.getInstance().getReference().child("Users");


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLinearLayout);

        recyclerView.setAdapter(adapter);

       if(usertype.equals("student")){
           popuStud();
       }
       if(usertype.equals("faculty")){
           popuFaculty();
       }

       if (usertype.equals("staff")){
           popuStaff();
       }

        return myFragment;
    }

    public void popuStud(){
        query=mDataMsgStudent.orderByChild("receiver").equalTo(usertype);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        //viewHolder.real.setBackgroundColor(Color.GREEN);
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    public void popuFaculty(){
        query=mDataMsgFaculty.orderByChild("receiver").equalTo(usertype);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        //viewHolder.real.setBackgroundColor(Color.GREEN);
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    public void popuStaff(){
        query=mDataMsgStaff.orderByChild("receiver").equalTo(usertype);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        //viewHolder.real.setBackgroundColor(Color.GREEN);
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        if(usertype.equals("student")){
            popuStud();
        }
        if(usertype.equals("faculty")){
            popuFaculty();
        }

        if (usertype.equals("staff")){
            popuStaff();
        }
        super.onStart();

    }
}
