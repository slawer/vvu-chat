package com.vvuchat.vvuchat.Fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.vvuchat.vvuchat.Adapter.MsgAdap;
import com.vvuchat.vvuchat.Model.Messages;
import com.vvuchat.vvuchat.Model.PrefConfig;
import com.vvuchat.vvuchat.R;


public class DeptMsgs extends Fragment {
    View myFragment;
    RecyclerView recyclerView;
    LinearLayoutManager mLinearLayout;
    FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder> adapter;

    PrefConfig prefConfig;
    public DatabaseReference mDataMsgStaff,mDataMsgStudent,mDataMsgFaculty,mUsers;

    private ProgressDialog mProgress;

    private FirebaseAuth mAuth;

    String user_id="";
    String usertype="";
    String school= "";
    String user_department= "";

    Query query;

    public DeptMsgs() {
        // Required empty public constructor
    }


    public static DeptMsgs newInstance() {
        DeptMsgs fragment = new DeptMsgs();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragment= inflater.inflate(R.layout.fragment_dept_msgs, container, false);
        recyclerView=myFragment.findViewById(R.id.recyclerDepartment);
        mLinearLayout = new LinearLayoutManager(getActivity());

        prefConfig=new PrefConfig(getActivity());
        mAuth= FirebaseAuth.getInstance();
        mProgress=new ProgressDialog(getActivity());

        user_id=mAuth.getCurrentUser().getUid();
        usertype=prefConfig.readType();
        school=prefConfig.readSchool();
        user_department=prefConfig.readDepartment();

        mDataMsgStudent= FirebaseDatabase.getInstance().getReference().child("StudentMessages");
        mDataMsgFaculty= FirebaseDatabase.getInstance().getReference().child("FacultyMessages");
        mDataMsgStaff= FirebaseDatabase.getInstance().getReference().child("StaffMessages");


        mUsers= FirebaseDatabase.getInstance().getReference().child("Users");



        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLinearLayout);

        recyclerView.setAdapter(adapter);

//        if((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("student")))){
//            popuStud();
//        }
//        if((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("faculty")))){
//            popuFaculty();
//        }
//
//        if ((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("staff")))){
//            popuStaff();
//        }

        mDataMsgFaculty.keepSynced(true);
        mDataMsgStaff.keepSynced(true);
        mDataMsgStudent.keepSynced(true);

        if(usertype.equals("student")){
            popuStud();
        }
        else if(usertype.equals("faculty")){
            popuFaculty();
        }
        else if(usertype.equals("staff")){
            popuStaff();
        }
        return myFragment;
    }

    public void popuStud(){
        query=mDataMsgStudent.orderByChild("department").equalTo(user_department);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        //viewHolder.real.setBackgroundColor(Color.GREEN);
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    public void popuFaculty(){
        query=mDataMsgFaculty.orderByChild("department").equalTo(user_department);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    public void popuStaff(){
        query=mDataMsgStaff.orderByChild("school").equalTo(school);
        adapter= new FirebaseRecyclerAdapter<Messages, MsgAdap.MsgViewHolder>(Messages.class, R.layout.receivedmessages, MsgAdap.MsgViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MsgAdap.MsgViewHolder viewHolder, Messages model, int position) {
                String current_user_id=mAuth.getCurrentUser().getUid();
                try {
                    String senderID=model.getSenderId();

                    if (senderID.equals(current_user_id)){
                        viewHolder.real.setBackgroundResource(R.drawable.btn_bg);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                    }
                    else{
                        //viewHolder.real.setBackgroundColor(Color.GREEN);
                        viewHolder.real.setBackgroundResource(R.drawable.text_background);
                        viewHolder.messageText.setTextColor(Color.WHITE);
                        viewHolder.messageUser.setTextColor(Color.WHITE);
                        viewHolder.messageTime.setTextColor(Color.WHITE);
                    }

                    viewHolder.messageText.setText(model.getMessage());
                    viewHolder.messageUser.setText(model.getSender());
                    viewHolder.messageTime.setText(model.getDatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

//    @Override
//    public void onStart() {
//        if((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("student")))){
//            popuStud();
//        }
//        if((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("faculty")))){
//            popuFaculty();
//        }
//
//        if ((school.equals(getResources().getString(R.string.sob)) || school.equals(getResources().getString(R.string.fass)) || school.equals(getResources().getString(R.string.soe)) || school.equals(getResources().getString(R.string.fos)) || school.equals(getResources().getString(R.string.sotm)) && (usertype.equals("staff")))){
//            popuStaff();
//        }
//        super.onStart();
//
//    }
}