package com.vvuchat.vvuchat.Service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.vvuchat.vvuchat.Common.Common;
import com.vvuchat.vvuchat.Model.Token;


/**
 * Created by nigma on 21/01/2018.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken= FirebaseInstanceId.getInstance().getToken();
        
        updateTokenServer(refreshedToken);
    }

    private void updateTokenServer(String refreshedToken) {
        FirebaseDatabase database= FirebaseDatabase.getInstance();
        DatabaseReference tokens=database.getReference(Common.token_table);

        Token token=new Token(refreshedToken);
        if(FirebaseAuth.getInstance().getCurrentUser()!=null)//updates token when user is already logged in
            tokens.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .setValue(token);

    }
}
