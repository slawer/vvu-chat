package com.vvuchat.vvuchat.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vvuchat.vvuchat.R;

import java.util.Map;

/**
 * Created by nigma on 21/01/2018.
 */

public class MyFirebaseMessaging extends FirebaseMessagingService{

    //COnverting the message into latlng in order to send to firebase
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        if(remoteMessage.getData()!=null) {
//
//            Map<String,String> data=remoteMessage.getData();
//            String customer=data.get("customer");
//            final String message=data.get("message");
//
//
//            Intent intent = new Intent(getBaseContext(), CustomerCall.class);
//            intent.putExtra("lat", customer_location.latitude);
//            intent.putExtra("lng", customer_location.longitude);
//            intent.putExtra("customer", customer);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            getBaseContext().startActivity(intent);
//        }

        if (remoteMessage.getData() != null) {

            Map<String,String> data=remoteMessage.getData();
            String title=data.get("position");
            final String message=data.get("message");

              showArrivalNotification(message);

        }
    }

//    private void openRateActivity(String body) {
//        Intent intent=new Intent(MyFirebaseMessaging.this, RatingActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    }

    private void showArrivalNotification(String body) {
        PendingIntent pendingIntent=PendingIntent.getActivity(getBaseContext(),0,new Intent(),PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(getBaseContext());

        builder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_LIGHTS|Notification.DEFAULT_SOUND)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("New Message")
                .setContentText(body)
                .setContentIntent(pendingIntent);

        NotificationManager manager=(NotificationManager)getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1,builder.build());

    }
}
